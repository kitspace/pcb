EESchema Schematic File Version 4
LIBS:polymer-cache
EELAYER 29 0
EELAYER END
$Descr A 11000 8500
encoding utf-8
Sheet 1 2
Title "Polymer"
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	5250 4300 4950 4300
Wire Wire Line
	5250 4200 4950 4200
Wire Wire Line
	5250 4400 4950 4400
Wire Wire Line
	5250 4700 4950 4700
Text Label 6300 3300 0    50   ~ 0
RX_R
Text Label 6300 3400 0    50   ~ 0
TX_R
Text Label 5000 4700 0    50   ~ 0
GND
Text Label 6350 3000 0    50   ~ 0
GND
Text Label 5000 4200 0    50   ~ 0
TX_L
Text Label 5000 4300 0    50   ~ 0
RX_L
Text Label 6300 3700 0    50   ~ 0
COL4
Text Label 6300 3800 0    50   ~ 0
COL5
Text Label 6300 3600 0    50   ~ 0
COL6
Text Label 5000 4400 0    50   ~ 0
ROW2
Text Label 6300 4400 0    50   ~ 0
ROW3
Text Label 6300 4500 0    50   ~ 0
ROW4
Text Label 6300 4600 0    50   ~ 0
ROW5
Wire Wire Line
	1550 3300 1200 3300
Text Label 1450 3300 2    50   ~ 0
COL1
Wire Wire Line
	1550 3450 1200 3450
Wire Wire Line
	1550 3600 1200 3600
Wire Wire Line
	1550 3750 1200 3750
Wire Wire Line
	1550 3900 1200 3900
Wire Wire Line
	1550 4050 1200 4050
Text Label 1450 3450 2    50   ~ 0
COL2
Text Label 1450 3600 2    50   ~ 0
COL3
Text Label 1450 3750 2    50   ~ 0
COL4
Text Label 1450 3900 2    50   ~ 0
COL5
Text Label 1450 4050 2    50   ~ 0
COL6
Wire Wire Line
	2750 3450 3050 3450
Wire Wire Line
	2750 3600 3050 3600
Wire Wire Line
	2750 3750 3050 3750
Wire Wire Line
	2750 3900 3050 3900
Wire Wire Line
	2750 4050 3050 4050
Text Label 3000 3450 2    50   ~ 0
ROW1
Text Label 3000 3600 2    50   ~ 0
ROW2
Text Label 3000 3750 2    50   ~ 0
ROW3
Text Label 3000 3900 2    50   ~ 0
ROW4
Text Label 3000 4050 2    50   ~ 0
ROW5
NoConn ~ 6250 4800
NoConn ~ 5250 3000
NoConn ~ 5250 3100
NoConn ~ 5250 2900
NoConn ~ 6250 4300
NoConn ~ 6250 4200
NoConn ~ 6250 4000
NoConn ~ 6250 3900
NoConn ~ 6250 3500
Wire Wire Line
	1550 4200 1200 4200
Text Label 1450 4200 2    50   ~ 0
COL7
Wire Wire Line
	6250 4100 6550 4100
Text Label 6300 4100 0    50   ~ 0
COL7
$Sheet
S 1550 3200 1200 1050
U 5BCB8B3B
F0 "Polymer-Matrix" 50
F1 "matrix.sch" 50
F2 "COL1" U L 1550 3300 50 
F3 "COL2" U L 1550 3450 50 
F4 "COL3" U L 1550 3600 50 
F5 "COL4" U L 1550 3750 50 
F6 "COL5" U L 1550 3900 50 
F7 "COL6" U L 1550 4050 50 
F8 "ROW1" U R 2750 3450 50 
F9 "ROW2" U R 2750 3600 50 
F10 "ROW3" U R 2750 3750 50 
F11 "ROW4" U R 2750 3900 50 
F12 "ROW5" U R 2750 4050 50 
F13 "COL7" U L 1550 4200 50 
$EndSheet
Wire Wire Line
	5250 4600 4950 4600
Text Label 5100 4600 2    50   ~ 0
5V
NoConn ~ 5250 4800
NoConn ~ 6250 3100
Text Label 6350 2900 0    50   ~ 0
GND
NoConn ~ 5250 4500
Wire Wire Line
	6550 4600 6250 4600
Wire Wire Line
	6550 4500 6250 4500
Wire Wire Line
	6550 4400 6250 4400
Wire Wire Line
	6250 3800 6550 3800
Wire Wire Line
	6250 3700 6550 3700
Wire Wire Line
	6250 3600 6550 3600
Wire Wire Line
	6250 3400 6550 3400
Wire Wire Line
	6250 3300 6550 3300
Wire Wire Line
	6250 3000 6550 3000
Wire Wire Line
	6250 2900 6550 2900
$Comp
L Connector:Conn_01x04_Male J1
U 1 1 5CECC26F
P 3950 1650
F 0 "J1" H 4058 1931 50  0000 C CNN
F 1 "JST LEFT" H 4058 1840 50  0000 C CNN
F 2 "Connector_JST:JST_SH_SM04B-SRSS-TB_1x04-1MP_P1.00mm_Horizontal" H 3950 1650 50  0001 C CNN
F 3 "~" H 3950 1650 50  0001 C CNN
	1    3950 1650
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Male J2
U 1 1 5CECE992
P 4950 1650
F 0 "J2" H 5058 1931 50  0000 C CNN
F 1 "JST RIGHT" H 5058 1840 50  0000 C CNN
F 2 "Connector_JST:JST_SH_SM04B-SRSS-TB_1x04-1MP_P1.00mm_Horizontal" H 4950 1650 50  0001 C CNN
F 3 "~" H 4950 1650 50  0001 C CNN
	1    4950 1650
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 1550 4600 1550
Wire Wire Line
	4150 1650 4600 1650
Wire Wire Line
	4150 1750 4600 1750
Wire Wire Line
	4150 1850 4600 1850
Wire Wire Line
	5150 1550 5600 1550
Wire Wire Line
	5150 1650 5600 1650
Wire Wire Line
	5150 1750 5600 1750
Wire Wire Line
	5150 1850 5600 1850
Text Label 5250 1750 0    50   ~ 0
RX_R
Text Label 5350 1650 0    50   ~ 0
5V
Text Label 5250 1850 0    50   ~ 0
TX_R
Text Label 5300 1550 0    50   ~ 0
GND
Text Label 4300 1850 0    50   ~ 0
RX_L
Text Label 4300 1750 0    50   ~ 0
TX_L
Text Label 4350 1650 0    50   ~ 0
5V
Text Label 4300 1550 0    50   ~ 0
GND
$Comp
L polymer:RobotDynSTM32 U1
U 1 1 5BCB8339
P 5750 4150
F 0 "U1" V 5750 2700 50  0000 C CNN
F 1 "RobotDynSTM32" V 5750 3850 50  0000 C CNN
F 2 "polymer:RobotDynSTM32" H 5800 5450 50  0001 C CNN
F 3 "" H 5800 5450 50  0001 C CNN
	1    5750 4150
	-1   0    0    1   
$EndComp
$Comp
L Switch:SW_Push PB1
U 1 1 5D094D6C
P 7250 1700
F 0 "PB1" H 7250 1985 50  0000 C CNN
F 1 "SW_Push" H 7250 1894 50  0000 C CNN
F 2 "SKSCLDE010:SKSCLDE010" H 7250 1900 50  0001 C CNN
F 3 "~" H 7250 1900 50  0001 C CNN
	1    7250 1700
	1    0    0    -1  
$EndComp
Text Label 7550 1700 0    50   ~ 0
GND
Wire Wire Line
	7450 1700 7750 1700
Wire Wire Line
	7050 1700 6800 1700
Text Label 6850 1700 0    50   ~ 0
RESET
Wire Wire Line
	6250 3200 6550 3200
Text Label 6300 3200 0    50   ~ 0
RESET
Text Label 5000 4100 0    50   ~ 0
ROW1
Wire Wire Line
	5250 4100 4950 4100
Text Label 5000 3400 0    50   ~ 0
COL3
Text Label 5000 3500 0    50   ~ 0
COL2
Text Label 5000 3300 0    50   ~ 0
COL1
Wire Wire Line
	4950 3500 5250 3500
Wire Wire Line
	4950 3400 5250 3400
Wire Wire Line
	4950 3300 5250 3300
NoConn ~ 5250 3200
Wire Wire Line
	5250 3600 4950 3600
Wire Wire Line
	5250 3700 4950 3700
Text Label 5000 3600 0    50   ~ 0
USBD-
Text Label 5000 3700 0    50   ~ 0
USBD+
NoConn ~ 4950 3600
NoConn ~ 4950 3700
Wire Wire Line
	5250 3800 4950 3800
Wire Wire Line
	5250 3900 4950 3900
Wire Wire Line
	5250 4000 4950 4000
NoConn ~ 4950 3800
NoConn ~ 4950 3900
NoConn ~ 4950 4000
Text Label 5000 4000 0    50   ~ 0
JNTRST
Text Label 5000 3900 0    50   ~ 0
JTDO
Text Label 5000 3800 0    50   ~ 0
JTDI
Wire Wire Line
	6250 4700 6550 4700
Text Label 6300 4700 0    50   ~ 0
LED
NoConn ~ 6550 4700
$EndSCHEMATC
